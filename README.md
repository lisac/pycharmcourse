An instructional course about classical ciphers, created for the [PyCharm Best Interactive Programming Course Contest](https://www.jetbrains.com/pycharm-educational/contest/).  The course contents have been exported to IntroToCiphers.zip.

With more time, I would add tasks that discuss vulnerabilities of these ciphers. To begin with, I would describe the brute force and frequency analysis attacks on the Caesar cipher; the python concepts that I'd exercise would be the 'for' loop and dictionaries. 

Additionally, I would add at least 2 more lessons, to be more fair to the breadth of classic ciphers. The two I'd next introduce would be the Vigenère substitution cipher and the rail fence transposition cipher.

Thanks for reading.